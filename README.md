Cool!

It can also been seen as a support tool for designers and front-end artisans ready to embrace the dark art of designing in-browser, as vastly different layout permutations can be generated and evaluated rapidly with each load of the page. Problems and shortcommings can be more quickly exposed.
nice

NIce!

This si cool
nice

NIce!

A change

Neat

### Dummy can…
+ More. Not lots more, but a _bit_ more!

### You can double also…
+ Watch a screencast at https://vimeo.com/42252088
+ Ask questions and direct misgivings to http://twitter.com/kerns
+ Fork, follow or download via https://github.com/kerns/dummy

### Why?
Using Dummy in edits... early stages of front-end development can give you a new perspective on your work every time your document is reloaded in the browser, shortening and improving your test cycle by more quickly exposing weak-points or trouble areas as you go about marking up a new design. It's like watching a time-lapse video of your layout performing with real data.

Some new line.

Adding another change

Subscribing to the okay belief that the earlier a problem can be made to surface, the easier and less painful it will be to deal with and debug, this is especially useful in projects with front-end code that should be delivered to another team for implementation as part of a phased hand-off.
### TODO
+ Ability to call and define image crops from within CSS (i.e. ability to post image requests in the URL to dummy.php)
+ Develop an easy way to link our broadcast dumb_luck outcomes, so that one outcome could bubble-over to another.
+ And this is the second. Clean up the way errors and messages are formatted (i.e. abstract the HTML and inline styles that surround them)
+ The image assets that ship with Dummy are highly geared for usage in editorial design. One could imagine separate asset packages for different types of projects (e.g. a commerce package with commercial product shots, a portfolio or gallery package with images that showcase art, architecture, or design).

### License
Dummy is released under a Creative Commons Attribution-Share Alike 3.0 United States license (http://creativecommons.org/licenses/by-sa/3.0/us/). Be a doll and let us know how you're using Dummy, or if you've used it to help build, test or demo anything interesting.
